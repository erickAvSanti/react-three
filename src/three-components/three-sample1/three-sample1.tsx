import { useEffect, useRef } from "react";
import { setupThree } from "./lib";

const ThreeSample1 = () => {
  const canvas1 = useRef(null);
  useEffect(() => {
    setupThree(canvas1.current);
  }, []);
  return <div>
    <canvas ref={canvas1}></canvas>
  </div>
}

export default ThreeSample1;