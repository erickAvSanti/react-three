import './App.css';
import ThreeSample1 from './three-components/three-sample1/three-sample1';

function App() {
  return (
    <ThreeSample1></ThreeSample1>
  );
}

export default App;
